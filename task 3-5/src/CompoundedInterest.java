import java.text.DecimalFormat;
import java.util.Scanner;

public class CompoundedInterest {

	public static void main(String[] args) {
		// Set variables
		double principal;
		double percentageRate;
		int periods;
		double interest;
		double totalCosts;
		
		// Set decimal format to 2 decimal places.
		DecimalFormat df = new DecimalFormat("#.00");
		
		// Create new scanner
		Scanner scan = new Scanner(System.in);
		
		// Get user input
		System.out.println("Put in the amount of money that is borrowed.");
		principal = scan.nextDouble();
		
		System.out.println("Put in the percentage rate (in percentages %)");
		percentageRate = scan.nextDouble();
		
		System.out.println("Put in the number of periods interest is paid over.");
		periods = scan.nextInt();
		
		// Calculate interest
		percentageRate /= 100;
		totalCosts = principal * Math.pow(1 + percentageRate, periods);
		interest = totalCosts - principal;
		
		// Give user output
		System.out.println("\nThe total costs of this loan are: $" + df.format(totalCosts));
		System.out.println("The interest you will have to pay is: $" + df.format(interest));
	}
}
