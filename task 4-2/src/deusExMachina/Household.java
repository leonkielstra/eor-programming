package deusExMachina;

import java.util.Random;

public class Household {

	private final FoodStuff whatIsMade;
	private int familyMembers;
	private int[] stocks = null;
	Random rand = null;

	public Household(FoodStuff produced) {
		whatIsMade = produced;
		rand = new Random();
		familyMembers = rand.nextInt(ExperimentalParameters.maxFamilySize)
				+ ExperimentalParameters.minFamilySize;
		System.out.println("household that produces " + produced + " has " + familyMembers + " persons");
		// initialize stocks
		stocks = new int[FoodStuff.values().length];

		for (FoodStuff food : FoodStuff.values()) {
			stocks[food.ordinal()] = 0;
		}

	}

	public void produceOwnFood() {
		int produceIndex = whatIsMade.ordinal();
		stocks[produceIndex] += ExperimentalParameters.productionPerDay;
	}

	public void givesSomeOfOwnProduceTo(BarterPlace underTree) {
		// they give all! because they will take back what they need
		underTree.add(whatIsMade, ExperimentalParameters.productionPerDay);
		stocks[whatIsMade.ordinal()] -= ExperimentalParameters.productionPerDay;

	}

	public void takesWhatItNeedsFrom(BarterPlace fromUnderTheTree) {
		for (FoodStuff food : FoodStuff.values()) {
			stocks[food.ordinal()] += fromUnderTheTree.takeAway(food,
					ExperimentalParameters.exchangePerDay);
		}
	}

	public void eatsADay() {
		for (FoodStuff food : FoodStuff.values()) {
			int consumption = rand.nextInt(ExperimentalParameters.maxConsumptionPerPersonPerDay)
					* familyMembers;
			if (stocks[food.ordinal()] >= consumption) {
				stocks[food.ordinal()] -= consumption;
			} else {
				int shortage = consumption - stocks[food.ordinal()];
				System.err.println("The household that produces " + this.whatIsMade + " has a shortage of " + shortage + " " + food.name());
				
				stocks[food.ordinal()] = 0;
			}
		}
	}

	public void reportStocks() {
		System.out.println("house producing " + whatIsMade + " has now");
		for (FoodStuff food : FoodStuff.values()) {
			System.out.print(food + " = " + stocks[food.ordinal()] + "\t\t");
		}
		System.out.println("\n");

	}
}
