package deusExMachina;

public class ExperimentalParameters {
	public static final int maxNoOfDays = 3;
	public static final int productionPerDay = 90;
	public static final int exchangePerDay = 25;
	public static final int maxConsumptionPerPersonPerDay = 4;
	public static final int maxFamilySize = 10;
	public static final int minFamilySize = 3;
}
