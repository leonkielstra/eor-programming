package deusExMachina;

public class BarterPlace {
	private int[] commonStocks = new int[FoodStuff.values().length];// what is put in common

	public BarterPlace() {
		// initialize common stocks on zero for all the foods
		for (FoodStuff food : FoodStuff.values()){
			commonStocks[food.ordinal()] = 0;
		}
	}

	public void doBarter(Household[] households) {
		// fill the commonStocks
		for (Household house : households) {
			house.givesSomeOfOwnProduceTo(this);
		}
		// take what you need
		for (Household house : households) {
			house.takesWhatItNeedsFrom(this);
		}
	}

	public void add(FoodStuff produce, int howMuch) {
		commonStocks[produce.ordinal()] += howMuch;
	}

	public int takeAway(FoodStuff produce, int need) {
		if (commonStocks[produce.ordinal()] >= need) {
			commonStocks[produce.ordinal()] -= need;
		} else {
			need = commonStocks[produce.ordinal()];
			commonStocks[produce.ordinal()] = 0;
			System.err.println("The need for " + produce + " is bigger than what is left under the tree.");
		}
		return need;
	}
	
	public void reportStocks(){
		for(FoodStuff food : FoodStuff.values()){
			System.out.println(food + " remaining under the tree = " + commonStocks[food.ordinal()]);
		}
		System.out.println();
	}

}
