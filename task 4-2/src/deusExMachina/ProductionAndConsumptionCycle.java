package deusExMachina;

public class ProductionAndConsumptionCycle {
	
	public static void main(String[] args) {
		// initialize objects and status
		Household[] households = new Household[FoodStuff.values().length];
		
		for (FoodStuff produce : FoodStuff.values()){
			households[produce.ordinal()] = new Household(produce);
		}

		BarterPlace underTree = new BarterPlace();

		int barteringCycle = 1;//cycle counter
		do {
			System.out.println("cycle " + barteringCycle + " starts");
			// produce food first
			for (Household house : households) {// for all households
				house.produceOwnFood();
			}
			System.out.println("\n after producing and BEFORE bartering, the houses have..");
			for (FoodStuff food : FoodStuff.values()){
				households[food.ordinal()].reportStocks();
			}
			// barter
			underTree.doBarter(households);
			// consume
			for (Household house : households) {// for all households
				house.eatsADay();
			}
			System.out.println("\n Barter and Consume Cycle " + barteringCycle + " ended\n");
			underTree.reportStocks();
			
			for (FoodStuff food : FoodStuff.values()){
				households[food.ordinal()].reportStocks();
			}
						
			barteringCycle++;
		} while (barteringCycle <= ExperimentalParameters.maxNoOfDays);

	}

}
