import java.util.Scanner;


public class CalculateInvestment {

	public static void main(String[] args) {
		// set variables
		int GDP;	// Gross Domestic Product
		int DC;		// Domestic Consumption
		int GS;		// Government Spending
		int NX;		// Net Exports
		int INV;	// Investment
		
		// create new scanner
		Scanner scan = new Scanner(System.in);
		
		// Get user input
		System.out.println("What is the value of the Gross Domestic Product (GDP - in million dollars)?");
		GDP = scan.nextInt();
		
		System.out.println("What is the value of the Domestic Consumption (DC - in million dollars)?");
		DC = scan.nextInt();
		
		System.out.println("What is the value of the Government Spending (GS - in million dollars)?");
		GS = scan.nextInt();
		
		System.out.println("What is the value of the Net Exports (NX - in million dollars)?");
		NX = scan.nextInt();
		
		// Calculate investment
		INV = GDP - DC - GS - NX;
		
		// Give output
		System.out.println("The potential investment INV = GDP - DV - GS - NX is " + INV + " million dollars");

	}

}
