import java.text.DecimalFormat;
import java.util.Scanner;


public class DiscountInstalmentLoan {
	
	public static void main(String[] args) {
		// Set variables
		double moneyNeeded;
		int interestRate;
		double numberOfPeriods;
		double faceValue;
		double monthlyPayment;
		
		// Set decimal format to 2 decimal places.
		DecimalFormat df = new DecimalFormat("#.00");
		
		// new scanner
		Scanner scan = new Scanner(System.in);
		
		// Get user input
		System.out.println("How much money do you need to borrow?");
		moneyNeeded = scan.nextDouble();
		
		System.out.println("What is the current interest rate (in percentage %)?");
		interestRate = scan.nextInt();
		
		System.out.println("Over how many periods are you going to pay back te loan (in years)?");
		numberOfPeriods = scan.nextDouble();
		
		// Do the calculations
		faceValue = moneyNeeded/(100-interestRate*numberOfPeriods)*100;
		monthlyPayment = faceValue/(numberOfPeriods*12);
		
		// Give user output
		System.out.println("\nThe face value of your loan will be: $" + df.format(faceValue));
		System.out.println("Your monthly payment will be: $" + df.format(monthlyPayment));
	}
}
