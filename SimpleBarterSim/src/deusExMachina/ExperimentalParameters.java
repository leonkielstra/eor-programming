package deusExMachina;

public class ExperimentalParameters {
	public static final int maxNoOfDays = 3;
	public static final int production = 90;
	public static final int exchange = 30;
	public static final int averageFamilySize = 10;
	public static int depletionRate = 2;
}
