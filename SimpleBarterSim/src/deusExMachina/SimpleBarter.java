package deusExMachina;

public class SimpleBarter {
	
	public static void main(String[] args) {
		// initialize objects and status
		
		Household householdOne = new Household(FoodStuff.TOMATO);
		Household householdTwo = new Household(FoodStuff.CHEESE);
		Household householdThree = new Household(FoodStuff.BREAD);

		int cycle = 1;//cycle counter
		do {
			System.out.println("cycle " + cycle + " starts");
			
			// produce food first
			int productionPerCycle = ExperimentalParameters.production;
			householdOne.tomatoStock += productionPerCycle;
			householdTwo.cheeseStock += productionPerCycle;
			householdThree.breadStock += productionPerCycle;
			
			System.out.println("\n after producing and BEFORE bartering, the houses have..");
			
			householdOne.reportStocks();
			householdTwo.reportStocks();
			householdThree.reportStocks();
			
			// barter
			int exchangeQuantity = ExperimentalParameters.exchange;
			//cheese for bread
			householdTwo.cheeseStock -= exchangeQuantity;
			householdThree.cheeseStock += exchangeQuantity;
			householdThree.breadStock -= exchangeQuantity;
			householdTwo.breadStock += exchangeQuantity;
			//bread for tomato
			householdThree.breadStock -= exchangeQuantity;
			householdOne.breadStock += exchangeQuantity;
			householdOne.tomatoStock -= exchangeQuantity;
			householdThree.tomatoStock += exchangeQuantity;
			//tomato for cheese
			householdOne.tomatoStock -= exchangeQuantity;
			householdTwo.tomatoStock += exchangeQuantity;
			householdTwo.cheeseStock -= exchangeQuantity;
			householdOne.cheeseStock += exchangeQuantity;
			
			// consume

			householdOne.eatsForSoMany();
			householdTwo.eatsForSoMany();
			householdThree.eatsForSoMany();
			
			System.out.println("\n Barter and Consume Cycle " + cycle + " ended\n");
			
			
			householdOne.reportStocks();
			householdTwo.reportStocks();
			householdThree.reportStocks();
						
			cycle++;
		} while (cycle <= ExperimentalParameters.maxNoOfDays);

	}

}
