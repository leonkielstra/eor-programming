package deusExMachina;

public class Household {
	
	private final FoodStuff whatIsMade;
	private final int familyMembers = ExperimentalParameters.averageFamilySize;
	
	public int cheeseStock = 0;
	public int breadStock = 0;
	public int tomatoStock = 0;

	public Household(FoodStuff produced) {
		whatIsMade = produced;
	}
	
	public void eatsForSoMany(){
		tomatoStock -= familyMembers * ExperimentalParameters.depletionRate ;
		cheeseStock -= familyMembers * ExperimentalParameters.depletionRate ;
		breadStock -= familyMembers * ExperimentalParameters.depletionRate ;
	}
	
	public void reportStocks(){
		System.out.println("house producing " + whatIsMade + " has now");
		System.out.print("TOMATO = " + tomatoStock + "\t\t");
		System.out.print("CHEESE = " + cheeseStock + "\t\t");
		System.out.print("BREAD = " + breadStock + "\t\t");
		System.out.println("\n");

	}
}
