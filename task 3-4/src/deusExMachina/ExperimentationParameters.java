package deusExMachina;

// ExperimentationParameters with positive outcomes only
public class ExperimentationParameters {
	static int surplusRate = 100;
	static int depletionRate = 8;
	static int exchangeVolume = 25;
	static int maxNoOfDays = 4;
	static int averageFamilySize = 3;
}

// ExperimentationParameters with negative outcomes only
/*
public class ExperimentationParameters {
	static int surplusRate = 30;
	static int depletionRate = 18;
	static int exchangeVolume = 30;
	static int maxNoOfDays = 4;
	static int averageFamilySize = 3;
}
*/

