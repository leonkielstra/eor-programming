package deusExMachina;

public class Household {
	FoodStuff produce;
	public int cheeseStock = 0;
	public int breadStock = 0;
	public int tomatoStock = 0;
	
	public Household(FoodStuff what){//constructor method
		this.produce = what;
	}
	public void eat(){
		cheeseStock -= ExperimentationParameters.depletionRate * ExperimentationParameters.averageFamilySize;
		breadStock -= ExperimentationParameters.depletionRate * ExperimentationParameters.averageFamilySize;
		tomatoStock -= ExperimentationParameters.depletionRate * ExperimentationParameters.averageFamilySize;
	}
	public void reportStocks(){
		System.out.println("house producing " + produce + " has now");
		System.out.print("TOMATO = " + tomatoStock + "\n");
		System.out.print("CHEESE = " + cheeseStock + "\n");
		System.out.print("BREAD = " + breadStock + "\n");
		System.out.println("\n");
	}
}
