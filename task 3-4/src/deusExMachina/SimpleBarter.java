package deusExMachina;

public class SimpleBarter {
	
	public static void main(String[] args) {
		// initialize objects and status
		
		Household farmers = new Household(FoodStuff.TOMATO);
		Household dairies = new Household(FoodStuff.CHEESE);
		Household bakers = new Household(FoodStuff.BREAD);

		int cycle = 1;//cycle counter
		do {
			System.out.println("cycle " + cycle + " starts");
			
			// produce food first
			int productionPerCycle = ExperimentationParameters.surplusRate;
			farmers.tomatoStock += productionPerCycle;
			dairies.cheeseStock += productionPerCycle;
			bakers.breadStock += productionPerCycle;
			
			System.out.println("\n after producing and BEFORE bartering, the houses have..");
			
			farmers.reportStocks();
			dairies.reportStocks();
			bakers.reportStocks();
			
			// barter
			int exchangeQuantity = ExperimentationParameters.exchangeVolume;
			//cheese for bread
			dairies.cheeseStock -= exchangeQuantity;
			bakers.cheeseStock += exchangeQuantity;
			bakers.breadStock -= exchangeQuantity;
			dairies.breadStock += exchangeQuantity;
			//bread for tomato
			bakers.breadStock -= exchangeQuantity;
			farmers.breadStock += exchangeQuantity;
			farmers.tomatoStock -= exchangeQuantity;
			bakers.tomatoStock += exchangeQuantity;
			//tomato for cheese
			farmers.tomatoStock -= exchangeQuantity;
			dairies.tomatoStock += exchangeQuantity;
			dairies.cheeseStock -= exchangeQuantity;
			farmers.cheeseStock += exchangeQuantity;
			
			// consume

			farmers.eat();
			dairies.eat();
			bakers.eat();
			
			System.out.println("\n Barter and Consume Cycle " + cycle + " ended\n");
			
			
			farmers.reportStocks();
			dairies.reportStocks();
			bakers.reportStocks();
						
			cycle++;
		} while (cycle <= ExperimentationParameters.maxNoOfDays);

	}

}
