package deusExMachina;

public class DependentHousehold {
	private static int houses = 0;
	private double currentCorveePerWeekPerAdult;
	private double[] assets = new double[AssetList.values().length];
	private int adults;
	public final int houseNo;

	public DependentHousehold(int adults) {
		for (AssetList asset : AssetList.values()) {
			assets[asset.ordinal()] = 0;
		}
		this.adults = adults;
		houses++;
		this.houseNo = houses;
		currentCorveePerWeekPerAdult = ExP.typicalCorveeHoursPerWeekPerAdult;
		assets[AssetList.SILVER.ordinal()] = ExP.typicalSilverQuantityPerSerfHousehold;
		assets[AssetList.TOWN_PRODUCT.ordinal()] = ExP.typicalGoodsQuantityPerSerf * adults;
	}

	public void reportAdultsAndBoundLandSurface() {
		System.out.println("serf house no " + this.houseNo + " has " + adults + " adults, and is bound to a surface of "
				+ assets[AssetList.RESIDENCE_LAND.ordinal()]);
	}

	public Exchange offers(AssetList what, AssetList whatFor) {
		Exchange offer = new Exchange(what, whatFor);
		double offeredQuantity;
		switch (what) {
		case LABOR:
			offeredQuantity = currentCorveePerWeekPerAdult * adults + ExP.corveeVariability(currentCorveePerWeekPerAdult * adults);
			break;
		case AGRI_PRODUCE:
			offeredQuantity = assets[AssetList.AGRI_PRODUCE.ordinal()];
			break;
		default:
			offeredQuantity = 0;
			break;
		}
		offer.setQuantityGiven(offeredQuantity);
		return offer;
	}

	public void acceptsPartOrWholeOf(Exchange offer) {
		double toAccept;
		switch (offer.getWhatIsGiven()) {
		case OBLIGATION:
			toAccept = currentCorveePerWeekPerAdult * adults * YearAfterYearLongTimeAgo.workingWeeksInAYear;
			break;
		case TOWN_PRODUCT:
			switch (offer.getWhatIsTaken()) {
			case SILVER:
				toAccept = calculateNeed(offer);
				break;
			default:
				toAccept = 0;
				break;
			}
			break;
		default:
			toAccept = 0;
			break;
		}
		offer.setQuantityGiven(toAccept);
		offer.setQuantityTaken(toAccept * offer.getPricePerUnit());
		// change your assets assets
		assets[offer.getWhatIsGiven().ordinal()] += toAccept;
		assets[offer.getWhatIsTaken().ordinal()] -= toAccept * offer.getPricePerUnit();

	}

	private double calculateNeed(Exchange offer) {
		/*
		 * this method defines the behaviour of the serf as a buyer of town
		 * goods. He has a good behaviour: he does not consume more than 1/3 of
		 * its silver reserves
		 */
		double need = adults * ExP.goodsDepletionPerSerfPerWeek();
		depleteGoodsWithTheSameAmountAs(need);
		double budgetForThisNeed = need * offer.getPricePerUnit();
		if (budgetForThisNeed > (assets[AssetList.SILVER.ordinal()] / 3.0)) {
			need = (assets[AssetList.SILVER.ordinal()] / 3.0) / offer.getPricePerUnit();
		}
		return need;
	}

	private void depleteGoodsWithTheSameAmountAs(double depletion) {// avoids a
																	// negative
																	// value
		if (depletion > assets[AssetList.TOWN_PRODUCT.ordinal()]) {
			assets[AssetList.TOWN_PRODUCT.ordinal()] = 0;
		} else {
			assets[AssetList.TOWN_PRODUCT.ordinal()] -= depletion;
		}

	}

	public void settlesTransactionOver(Exchange exchange) {
		assets[exchange.getWhatIsGiven().ordinal()] -= exchange.getQuantityGiven();// deliver
																					// goods
		assets[exchange.getWhatIsTaken().ordinal()] += exchange.getQuantityTaken();// get
																					// paid
	}

	public void boundTo(double parcelForSerf) {
		assets[AssetList.RESIDENCE_LAND.ordinal()] += parcelForSerf;
	}

	public void computeYearlyLaborPotential(ExchangeRegulator theChurch) {
		double total = assets[AssetList.LABOR.ordinal()] = adults * theChurch.regulatedWorkingWeekHoursForSerfs()
				* YearAfterYearLongTimeAgo.workingWeeksInAYear;
		// System.out.println("serf house no " + houseNo +
		// " can work this year " + total + " hours");
		currentCorveePerWeekPerAdult = theChurch.regulatesCorvee(total, assets[AssetList.OBLIGATION.ordinal()], adults);
		// System.out.println("serf house no " + houseNo +
		// " does person corvee per week " + currentCorveePerWeekPerAdult);

	}

	public void reportsObligation() {
		System.out.println("serf house no " + houseNo + " still has to do corvee this year for " + assets[AssetList.OBLIGATION.ordinal()]
				+ " hours");

	}

	public void reportsRemainingProduce() {
		System.out.println("serf house no " + houseNo + " still has " + assets[AssetList.AGRI_PRODUCE.ordinal()] + " produce");

	}

	public void reportsSilver() {
		System.out.println("serf house no " + houseNo + " has a quantity of " + assets[AssetList.SILVER.ordinal()] + " silver");
	}

	public void reportsGoods() {
		System.out.println("serf house no " + houseNo + " has a quantity of " + assets[AssetList.TOWN_PRODUCT.ordinal()] + " goods");

	}

	public void produces(int currentWeekNo) {
		assets[AssetList.AGRI_PRODUCE.ordinal()] += ExP.productionIf(assets[AssetList.RESIDENCE_LAND.ordinal()],
				assets[AssetList.LABOR.ordinal()] / (YearAfterYearLongTimeAgo.workingWeeksInAYear + 2));
		assets[AssetList.LABOR.ordinal()] -= assets[AssetList.LABOR.ordinal()]
				/ (YearAfterYearLongTimeAgo.workingWeeksInAYear - currentWeekNo + 2);
		// System.out.println("serf house no " + houseNo + " produced " +
		// production);
	}

	public void consumes() {
		if (ExP.weeklyRationForSerfs * adults > assets[AssetList.AGRI_PRODUCE.ordinal()]) {
			assets[AssetList.AGRI_PRODUCE.ordinal()] = 0;
		} else {
			assets[AssetList.AGRI_PRODUCE.ordinal()] -= ExP.weeklyRationForSerfs * adults;
		}
		// System.out.println("serf house no " + houseNo + " still has " +
		// assets[AssetList.AGRI_PRODUCE.ordinal()]);
	}
	
	public int getAdults() {
		return adults;
	}
}
