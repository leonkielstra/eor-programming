package deusExMachina;

public class ExchangeRegulator {
	private static final double exchangeRateSilverTownProduce = 3;
	private static final double exchangeRateSilverAgriProduce = 3.5;
	private static final double exchangeRateObligations = 2;//this sets a negative obligation for the serf
	private static final double exchangeRateLaborForObligations = -1;
	private static final double workingWeekForSerf = 80;
	private static final double corveeFraction = 0.25;

	public void regulatesThe(Exchange offer) {
		// we can have a matrix of prices here, or a nested switch structure
		AssetList offeredAsset = offer.getWhatIsGiven();
		AssetList assetToBeAccepted = offer.getWhatIsTaken();
		double exchangeRate;
		switch (offeredAsset) {
		case AGRI_PRODUCE:
			switch (assetToBeAccepted) {
			case SILVER:
				exchangeRate = exchangeRateSilverAgriProduce;
				break;
			default:
				exchangeRate = 0;
				break;
			}
			break;
		case OBLIGATION:
			switch (assetToBeAccepted) {
			case OBLIGATION:
				exchangeRate = exchangeRateObligations;
			default:
				exchangeRate = 0;
				break;
			}
			break;
		case LABOR:
			switch (assetToBeAccepted) {
			case OBLIGATION:
				exchangeRate = exchangeRateLaborForObligations;
				break;
			default:
				exchangeRate = 0;
				break;
			}
			break;
		case TOWN_PRODUCT:
			switch (assetToBeAccepted) {
			case SILVER:
				exchangeRate = exchangeRateSilverTownProduce;
				break;
			default:
				exchangeRate = 0;
				break;
			}
			break;
		default:
			exchangeRate = 0;
			break;
		}
		offer.setPricePerUnit(exchangeRate);
	}

	public double regulatedWorkingWeekHoursForSerfs() {
		return workingWeekForSerf;//this can change from year to year
	}

	public double regulatesCorvee(double labor, double obligation, int persons) {
		return corveeFraction * (labor + obligation) / YearAfterYearLongTimeAgo.workingWeeksInAYear / persons;
	}
}
