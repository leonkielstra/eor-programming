package deusExMachina;

public class Exchange {
	private AssetList whatIsGiven;
	private double quantityGiven;
	private AssetList whatIsTaken;
	private double quantityTaken;
	private double pricePerUnit;

	public double getPricePerUnit() {
		return pricePerUnit;
	}

	public void setPricePerUnit(double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	public Exchange(AssetList given, AssetList taken) {
		whatIsGiven = given;
		whatIsTaken = taken;
	}

	public double getQuantityGiven() {
		return quantityGiven;
	}

	public void setQuantityGiven(double quantityGiven) {
		this.quantityGiven = quantityGiven;
	}

	public double getQuantityTaken() {
		return quantityTaken;
	}

	public void setQuantityTaken(double quantityTaken) {
		this.quantityTaken = quantityTaken;
	}

	public AssetList getWhatIsGiven() {
		return whatIsGiven;
	}

	public AssetList getWhatIsTaken() {
		return whatIsTaken;
	}

}
