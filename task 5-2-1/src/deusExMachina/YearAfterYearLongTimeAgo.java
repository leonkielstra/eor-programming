package deusExMachina;

public class YearAfterYearLongTimeAgo {
	// the economic actors are defined as static variables
	static ManorHouse leSeigneur;
	static DependentHousehold[] serfs;
	static ExchangeRegulator theChurch;
	static ExternalTradingPartner townMerchants;
	static final int workingWeeksInAYear = 52;

	public static void main(String[] args) {
		// economic actors are created
		theChurch = new ExchangeRegulator();
		leSeigneur = new ManorHouse(ExP.manorialDomainSurface, ExP.adultsAtManor);
		townMerchants = new ExternalTradingPartner();
		newSerfsAreCreated();

		int week, year = ExP.startYear;
		while (year <= ExP.endOfTheWorldYear) {
			System.out.print(year + " starts");
			establishHowMuchEachSerfHouseCanWorkThisYear();
			week = 1;
			do {
				// System.out.println("---week " + week + "starts\n");
				sellWeeklyCorveeLaborToSeigneurAndReduceObligations();
				produceForAWeek(week);// all economic actors are producing
										// agricultural produce
				consumeForAWeek();// some of this produce is consumed
				tradeAtTheLocalMarket();// the rest is traded, for silver and
										// town goods
				tradeAtTheTownMarket();
				week++;
			} while (week <= workingWeeksInAYear);
			System.out.println("-----and ends ");
			// leSeigneur.reportsObligation();
			year++;
		}
		reportEconomicalSituationOfSerfs();
	}

	private static void reportEconomicalSituationOfSerfs() {
		for (DependentHousehold serfHousehold : serfs) {
			// serfHousehold.reportsObligation();
			// serfHousehold.reportsRemainingProduce();
			// serfHousehold.reportsGoods();
			serfHousehold.reportsSilver();
		}
	}

	private static void tradeAtTheLocalMarket() {
		// first, Seigneur buys the surplus from his serfs, and pays with Silver
		for (DependentHousehold serfHousehold : serfs) {
			Exchange surplus = serfHousehold.offers(AssetList.AGRI_PRODUCE, AssetList.SILVER);
			theChurch.regulatesThe(surplus);
			leSeigneur.acceptsPartOrWholeOf(surplus);
			serfHousehold.settlesTransactionOver(surplus);
			// serfHousehold.reportsRemainingProduce();
			// serfHousehold.reportsSilver();
		}
		// after that Seigneur, sells manufactured goods from his reserves, and
		// the serfs pay with silver
		for (DependentHousehold serfHousehold : serfs) {
			Exchange goods = leSeigneur.offers(AssetList.TOWN_PRODUCT,/*for*/ AssetList.SILVER, 0);
			theChurch.regulatesThe(goods);
			serfHousehold.acceptsPartOrWholeOf(goods);
			leSeigneur.settlesTransactionOver(goods);
		}
	}

	private static void produceForAWeek(int weekNo) {
		leSeigneur.produces();
		for (DependentHousehold serfHousehold : serfs) {
			serfHousehold.produces(weekNo);
		}

	}

	private static void consumeForAWeek() {
		leSeigneur.consumes();
		for (DependentHousehold serfHousehold : serfs) {
			serfHousehold.consumes();
		}
	}

	private static void sellWeeklyCorveeLaborToSeigneurAndReduceObligations() {
		Exchange corvee;
		for (DependentHousehold serfHousehold : serfs) {
			corvee = serfHousehold.offers(AssetList.LABOR,/* in exchange for */AssetList.OBLIGATION);
			theChurch.regulatesThe(corvee);
			leSeigneur.acceptsPartOrWholeOf(corvee);
			serfHousehold.settlesTransactionOver(corvee);
			// serfHousehold.reportsObligation();
		}
		// leSeigneur.reportsObligation();

	}

	private static void establishHowMuchEachSerfHouseCanWorkThisYear() {
		Exchange obligation;
		for (DependentHousehold serfHousehold : serfs) {
			serfHousehold.computeYearlyLaborPotential(theChurch);
			obligation = leSeigneur.offers(AssetList.OBLIGATION, AssetList.OBLIGATION, 0);
			theChurch.regulatesThe(obligation);
			serfHousehold.acceptsPartOrWholeOf(obligation);
			leSeigneur.settlesTransactionOver(obligation);
			// serfHousehold.reportsObligation();
		}
		// leSeigneur.reportsObligation();
	}

	private static void newSerfsAreCreated() {
		serfs = new DependentHousehold[ExP.numberOfDependentHouseholds];
		for (int i = 0; i < serfs.length; i++) {
			serfs[i] = new DependentHousehold(ExP.makeSerfs());
			leSeigneur.allocatesLandForResidence(serfs[i]);
		}
		/*
		 * each serf household creation will diminish the directly owned land of
		 * the manor house, which will remain with a part called demesne
		 */
		for (DependentHousehold serf : serfs) {
			serf.reportAdultsAndBoundLandSurface();
		}
		System.out.println("Manor House has a demesne of " + leSeigneur.getDemesne());
	}
	
	private static void tradeAtTheTownMarket() {
		// leSeigneur trades agriculture products for silver. And the church regulates this.
		int reserves = 0;
		
		for(DependentHousehold dependentHousehold : serfs) {
			reserves += dependentHousehold.getAdults() * ExP.weeklyRationForSerfs;
		}
		
		reserves += leSeigneur.getAdults() * ExP.weeklyRationManorPeople;
		
		Exchange agricultureProducts = leSeigneur.offers(AssetList.AGRI_PRODUCE, AssetList.SILVER, reserves);
		theChurch.regulatesThe(agricultureProducts);
		townMerchants.acceptsPartOrWholeOf(agricultureProducts);
		leSeigneur.settlesTransactionOver(agricultureProducts);
		
		// townMerchants sell town products for silver to leSeigneur.
		Exchange townProducts = townMerchants.offers(AssetList.TOWN_PRODUCT, AssetList.SILVER);
		theChurch.regulatesThe(townProducts);
		leSeigneur.acceptsPartOrWholeOf(townProducts);
		townMerchants.acceptsPartOrWholeOf(townProducts);
	}

}
