package deusExMachina;

public class ManorHouse {
	private double[] assets = new double[AssetList.values().length];
	private int adults;

	public ManorHouse(double ownedLandSurface, int adults) {
		for (AssetList asset : AssetList.values()) {
			assets[asset.ordinal()] = 0;
		}
		this.adults = adults;
		assets[AssetList.DIRECTLY_OWNED_LAND.ordinal()] = ownedLandSurface;
		assets[AssetList.SILVER.ordinal()] = ExP.silver;
		assets[AssetList.TOWN_PRODUCT.ordinal()] = Integer.MAX_VALUE;
	}

	public Exchange offers(AssetList what, AssetList whatFor, int reserves) {
		Exchange offer = new Exchange(what, whatFor);
		double offeredQuantity;
		
		switch (what) {
		case OBLIGATION:
			offeredQuantity = Integer.MAX_VALUE;// he wants as much as possible
			break;
		case AGRI_PRODUCE:
			if (reserves > assets[AssetList.AGRI_PRODUCE.ordinal()]) {
				offeredQuantity = 0;
				System.err.println("Warning, your agriculture stocks are zero.");
			} else {
				offeredQuantity = assets[AssetList.AGRI_PRODUCE.ordinal()] - reserves;
			}
			
			break;
		default:
			offeredQuantity = 0;
			break;
		}
		offer.setQuantityGiven(offeredQuantity);
		
		
		return offer;
	}

	public void acceptsPartOrWholeOf(Exchange offer) {
		double toAccept;
		switch (offer.getWhatIsGiven()) {
		case LABOR:
			toAccept = offer.getQuantityGiven();// all corvee labor has to be
												// accepted
			break;
		case AGRI_PRODUCE:
			switch (offer.getWhatIsTaken()) {
			case SILVER:
				toAccept = offer.getQuantityGiven();// all the produce has to be
													// accepted, silver reserves
													// are near infinite;
				break;
			default:
				toAccept = 0;
				break;
			}
			break;
		default:
			toAccept = 0;
			break;
		}
		offer.setQuantityGiven(toAccept);
		offer.setQuantityTaken(toAccept * offer.getPricePerUnit());
		// change your assets assets
		assets[offer.getWhatIsGiven().ordinal()] += toAccept;
		assets[offer.getWhatIsTaken().ordinal()] -= toAccept * offer.getPricePerUnit();

	}

	public void settlesTransactionOver(Exchange exchange) {
		assets[exchange.getWhatIsGiven().ordinal()] -= exchange.getQuantityGiven();
		assets[exchange.getWhatIsTaken().ordinal()] += exchange.getQuantityTaken();
	}

	public void allocatesLandForResidence(DependentHousehold serf) {
		double surfaceGivenForResidence = ExP.createParcelForSerf();
		serf.boundTo(surfaceGivenForResidence);
		assets[AssetList.DIRECTLY_OWNED_LAND.ordinal()] -= surfaceGivenForResidence;
	}

	public double getDemesne() {
		return assets[AssetList.DIRECTLY_OWNED_LAND.ordinal()];
	}

	public void reportsObligation() {
		System.out.println("----Manor house still has a total demand for corvee this year of " + (-assets[AssetList.OBLIGATION.ordinal()])
				+ " hours");

	}

	public void produces() {
		double production = assets[AssetList.AGRI_PRODUCE.ordinal()] += ExP.productionIf(this.getDemesne(),
				assets[AssetList.LABOR.ordinal()]);
		assets[AssetList.LABOR.ordinal()] = 0;
		// System.out.println(" --- Manor House Produces " + production);
	}

	public void consumes() {
		if (ExP.weeklyRationManorPeople * adults > assets[AssetList.AGRI_PRODUCE.ordinal()]) {
			assets[AssetList.AGRI_PRODUCE.ordinal()] = 0;
		} else {
			assets[AssetList.AGRI_PRODUCE.ordinal()] -= ExP.weeklyRationManorPeople * adults;
		}
		// System.out.println(" --- Manor House still has after consuming some this week "
		// + assets[AssetList.AGRI_PRODUCE.ordinal()]);
	}
	
	public int getAdults() {
		return adults;
	}
}
