package deusExMachina;

import java.util.Random;

public class ExP {
	public final static int startYear = 893;
	public final static int endOfTheWorldYear = 1000;
	public final static int adultsAtManor = 20;
	public final static int numberOfDependentHouseholds = 13;
	public final static double typicalParcelOfSerfResidence = 9;
	public final static double typicalDemesneFraction = 0.37;
	public final static double manorialDomainSurface = numberOfDependentHouseholds * typicalParcelOfSerfResidence
			+ numberOfDependentHouseholds * typicalParcelOfSerfResidence * typicalDemesneFraction;
	public final static int typicalCorveeHoursPerWeekPerAdult = 20;
	public final static double weeklyRationManorPeople = 7;
	public final static double weeklyRationForSerfs = 3;	
	public final static double typicalSilverQuantityPerSerfHousehold = 50;
	public final static double typicalGoodsQuantityPerSerf = 10;
	public final static double silver = 5000;
	
	private static Random rand = new Random();
	
	public static int makeSerfs() {
		final double typicalNumberOfSerfsPerHousehold = 8;
		final double serfsPerHouseStDev = 6;
		final int minNumberOfSerfs = 2;
		int madeSerfs = (int) (rand.nextGaussian() * serfsPerHouseStDev + typicalNumberOfSerfsPerHousehold);
		if (madeSerfs < minNumberOfSerfs) {
			return minNumberOfSerfs;
		} else {
			return madeSerfs;
		}
	}

	public static double createParcelForSerf() {
		double parcelStDev = 3;
		double minParcel = 5;
		double parcel = rand.nextGaussian() * parcelStDev + typicalParcelOfSerfResidence;
		if (parcel < minParcel) {
			return minParcel;
		} else {
			return parcel;
		}
	}

	public static double corveeVariability(double total) {
		double stochasticPart = 0.03 * total * rand.nextGaussian();
		if (stochasticPart < (-1.0 / 3) * total) {
			return (-1.0 / 3) * total;
		}
		if (stochasticPart > (1.0 / 3) * total) {
			return (1.0 / 3) * total;
		}
		return stochasticPart;
	}
	public static double productionIf(double surfaceIs, double hoursAre){
		return surfaceIs * hoursAre / 100;
	}

	public static double goodsDepletionPerSerfPerWeek() {
		double depletion = 1.2;
		double stochasticPart = 0.09 * depletion * rand.nextGaussian();
		if (stochasticPart < (-1.0 / 3) * depletion) {
			return (-1.0 / 3) * depletion + depletion;
		}
		if (stochasticPart > (1.0 / 3) * depletion) {
			return (1.0 / 3) * depletion + depletion;
		}
		return stochasticPart + depletion;

	}

}
