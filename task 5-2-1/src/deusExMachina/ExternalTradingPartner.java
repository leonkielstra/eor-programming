package deusExMachina;

public class ExternalTradingPartner {
	private double[] assets = new double[AssetList.values().length];
	
	public ExternalTradingPartner() {
		for (AssetList asset : AssetList.values()) {
			assets[asset.ordinal()] = 0;
		}

		assets[AssetList.SILVER.ordinal()] = Integer.MAX_VALUE;
		assets[AssetList.TOWN_PRODUCT.ordinal()] = Integer.MAX_VALUE;
	}
	
	public Exchange offers(AssetList what, AssetList whatFor) {
		Exchange offer = new Exchange(what, whatFor);
		double offeredQuantity;
		switch (what) {
		case OBLIGATION:
			offeredQuantity = Integer.MAX_VALUE;// he wants as much as possible
			break;
		case TOWN_PRODUCT:
			offeredQuantity = assets[AssetList.TOWN_PRODUCT.ordinal()];
			/*
			 * offers everything each time, because the reserves are near
			 * infinite.
			 */
			break;
		default:
			offeredQuantity = 0;
			break;
		}
		offer.setQuantityGiven(offeredQuantity);
		return offer;
	}

	public void acceptsPartOrWholeOf(Exchange offer) {
		double toAccept;
		switch (offer.getWhatIsGiven()) {
		case LABOR:
			toAccept = offer.getQuantityGiven();// all corvee labor has to be
												// accepted
			break;
		case AGRI_PRODUCE:
			switch (offer.getWhatIsTaken()) {
			case SILVER:
				toAccept = offer.getQuantityGiven();// all the produce has to be
													// accepted, silver reserves
													// are near infinite;
				break;
			default:
				toAccept = 0;
				break;
			}
			break;
		default:
			toAccept = 0;
			break;
		}
		offer.setQuantityGiven(toAccept);
		offer.setQuantityTaken(toAccept * offer.getPricePerUnit());
		// change your assets assets
		assets[offer.getWhatIsGiven().ordinal()] += toAccept;
		assets[offer.getWhatIsTaken().ordinal()] -= toAccept * offer.getPricePerUnit();

	}

	public void settlesTransactionOver(Exchange exchange) {
		assets[exchange.getWhatIsGiven().ordinal()] -= exchange.getQuantityGiven();
		assets[exchange.getWhatIsTaken().ordinal()] += exchange.getQuantityTaken();
	}
}
