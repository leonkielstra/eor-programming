/*
 * A program to list the good things in life
 * Author: Barry Burd, BeginProg3@allmycode.com
 * February 13, 2012
 */
 
class ThingsILike {

  public static void main(String args[]) {
    System.out.println("Chocolate, royalties, sleep");
  } 
}
