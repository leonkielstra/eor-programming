
public class MathematicalComparison {
	
	public static void main(String[] args) {
		// Declare and initialize variables
		int number = 64;
		double smallValue = Math.pow(10, -20);	// The very small number can be adjusted here.
		
		double fraction = 1.3 * smallValue;
		double leftSide = fraction * number;
		double leftSideSquared = leftSide * leftSide;
		double rightSide = Math.sqrt(leftSideSquared);
		
		// check whether the leftSide and rightSide match
		if (leftSide == rightSide) {
			System.out.println(leftSide + " = " + rightSide);
			System.out.println("The left and right sides are Equal");
		} else {
			System.out.println("The left and right are NOT Equal");
		}
	}

}
