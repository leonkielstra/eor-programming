import java.util.Scanner;


public class RevertLines {

	public static void main(String[] args) {
		// define variables
		String firstLine;
		String secondLine;
		
		// setup scanner		
		Scanner scan = new Scanner(System.in);
		
		// Get the first and second line
		System.out.println("Put in the line you want to display last.");
		firstLine = scan.nextLine();
		
		System.out.println("Put in the line you want to display first.");
		secondLine = scan.nextLine();
		
		// Echo both lines reverted order
		System.out.println("\n" + secondLine);
		System.out.println(firstLine);
	}

}
